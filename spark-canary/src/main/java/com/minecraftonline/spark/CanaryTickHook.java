package com.minecraftonline.spark;

import me.lucko.spark.common.sampler.tick.AbstractTickHook;
import net.canarymod.Canary;
import net.canarymod.hook.HookHandler;
import net.canarymod.hook.system.ServerTickHook;
import net.canarymod.plugin.Plugin;
import net.canarymod.plugin.PluginListener;

public class CanaryTickHook extends AbstractTickHook implements PluginListener {
    private final Plugin plugin;

    public CanaryTickHook(Plugin plugin) {
        this.plugin = plugin;
    }

    @HookHandler
    public void handleTick(ServerTickHook hook) {
        onTick();
    }

    @Override
    public void start() {
        plugin.registerListener(this);
    }

    @Override
    public void close() {
        Canary.hooks().unregisterPluginListener(this);
    }
}
