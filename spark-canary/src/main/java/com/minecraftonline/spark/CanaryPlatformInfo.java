package com.minecraftonline.spark;

import me.lucko.spark.common.platform.AbstractPlatformInfo;
import net.canarymod.Canary;

public class CanaryPlatformInfo extends AbstractPlatformInfo {
    @Override
    public Type getType() {
        return Type.SERVER;
    }

    @Override
    public String getName() {
        return Canary.getImplementationTitle();
    }

    @Override
    public String getVersion() {
        return Canary.getImplementationVersion();
    }

    @Override
    public String getMinecraftVersion() {
        return Canary.getServer().getServerVersion();
    }
}
