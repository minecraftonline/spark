package com.minecraftonline.spark;

import me.lucko.spark.common.command.sender.AbstractCommandSender;
import net.canarymod.api.chat.ChatComponent;
import net.canarymod.api.entity.living.humanoid.Player;
import net.canarymod.chat.MessageReceiver;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.serializer.gson.GsonComponentSerializer;
import net.minecraft.util.IChatComponent;

import java.util.UUID;

public class CanaryCommandSender extends AbstractCommandSender<MessageReceiver> {
    public CanaryCommandSender(MessageReceiver delegate) {
        super(delegate);
    }

    @Override
    public String getName() {
        return delegate.getName();
    }

    @Override
    public UUID getUniqueId() {
        return delegate instanceof Player
                ? ((Player) delegate).getUUID()
                : null;
    }

    @Override
    public void sendMessage(Component message) {
        ChatComponent component = IChatComponent.Serializer.a(GsonComponentSerializer.colorDownsamplingGson().serialize(message)).getWrapper();
        if (delegate instanceof Player) {
            ((Player) delegate).sendChatComponent(component);
        } else {
            delegate.message(component.getFullText());
        }
    }

    @Override
    public boolean hasPermission(String permission) {
        return delegate.hasPermission(permission);
    }
}
