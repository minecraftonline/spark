package com.minecraftonline.spark;

import com.google.common.util.concurrent.ThreadFactoryBuilder;
import me.lucko.spark.common.SparkPlatform;
import me.lucko.spark.common.SparkPlugin;
import me.lucko.spark.common.command.sender.CommandSender;
import me.lucko.spark.common.platform.PlatformInfo;
import me.lucko.spark.common.sampler.ThreadDumper;
import me.lucko.spark.common.sampler.tick.TickHook;
import me.lucko.spark.common.sampler.tick.TickReporter;

import net.canarymod.Canary;
import net.canarymod.chat.MessageReceiver;
import net.canarymod.commandsys.*;
import net.canarymod.plugin.Plugin;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.stream.Stream;

public class CanarySparkPlugin extends Plugin implements SparkPlugin, CommandListener {
    protected final ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor(
            new ThreadFactoryBuilder().setNameFormat("spark-canary-async-worker").build()
    );

    private SparkPlatform platform;
    private TickHook hook;
    private TickReporter reporter;

    @Override
    public boolean enable() {
        this.platform = new SparkPlatform(this);
        this.platform.enable();

        try {
            registerCommands(this, true);
        } catch (CommandDependencyException e) {
            getLogman().error("Unable to register commands", e);
            return false;
        }

        return true;
    }

    @Override
    public void disable() {
        this.platform.disable();
    }

    @Command(
            aliases = "spark",
            permissions = "spark",
            description = "Main plugin command",
            toolTip = "",
            version = 2
    )
    public void handleCommand(MessageReceiver caller, String[] args) {
        platform.executeCommand(new CanaryCommandSender(caller), args);
    }

    @TabComplete(commands = "spark")
    public List<String> tabCompleteCommand(MessageReceiver caller, String[] args) {
        return platform.tabCompleteCommand(new CanaryCommandSender(caller), args);
    }

    public SparkPlatform getPlatform() {
        return this.platform;
    }

    @Override
    public PlatformInfo getPlatformInfo() {
        return null;
    }

    @Override
    public Path getPluginDirectory() {
        return Paths.get("config", getName());
    }

    @Override
    public String getCommandName() {
        return "spark";
    }

    @Override
    public Stream<? extends CommandSender> getSendersWithPermission(String permission) {
        return Stream.concat(
                Canary.getServer().getPlayerList().stream().filter(p -> p.hasPermission(permission)),
                Stream.of(Canary.getServer())
        ).map(CanaryCommandSender::new);
    }

    @Override
    public void executeAsync(Runnable task) {
        this.scheduler.execute(task);
    }

    @Override
    public ThreadDumper getDefaultThreadDumper() {
        return new ThreadDumper.Specific(new long[]{Thread.currentThread().getId()});
    }

    @Override
    public TickHook createTickHook() {
        return new CanaryTickHook(this);
    }

    @Override
    public TickReporter createTickReporter() {
        return new CanaryTickReporter(this);
    }
}
