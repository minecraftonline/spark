package com.minecraftonline.spark;

import me.lucko.spark.common.sampler.tick.AbstractTickReporter;
import net.canarymod.Canary;
import net.canarymod.hook.HookHandler;
import net.canarymod.hook.system.ServerTickHook;
import net.canarymod.plugin.Plugin;
import net.canarymod.plugin.PluginListener;

public class CanaryTickReporter extends AbstractTickReporter implements PluginListener {
    private final Plugin plugin;

    public CanaryTickReporter(Plugin plugin) {
        this.plugin = plugin;
    }

    @HookHandler
    public void handleTick(ServerTickHook hook) {
        // deltaTime is in nanoseconds, onTick expects milliseconds
        onTick(hook.getDeltaTime() / 1e6);
    }

    @Override
    public void start() {
        plugin.registerListener(this);
    }

    @Override
    public void close() {
        Canary.hooks().unregisterPluginListener(this);
    }
}
